This patch forcefully rips out references to all the I*.java interface files
that are not DFSG-free, having a "no military use" restriction.

See: http://lists.alioth.debian.org/pipermail/debian-med-packaging/2014-April/025987.html

However, it is ambiguous whether the files uder hep/aida/ref and hep/aida/bin are also
under the non-free license.  Only Wolfgang Hoschek can answer this question and he has been
sadly silent so far.  So I don't know if this patch actually solves anything or not.

The code is (or should be) functionally equivalent since the interface defs contain no actual code,
only function and class prototypes, and the process of removing them was mostly a matter of
find-and-replace of the interface names with the implementing class names.  However the code
is certainly less neat after this patch - especially as I forced VariableAxis to inherit from
FixedAxis - and also any code which links against Colt and references the I* classes will fail
to build and will require patching too.

-- Tim Booth <tbooth@ceh.ac.uk>, April 2014

--- a/src/hep/aida/ref/Util.java
+++ b/src/hep/aida/ref/Util.java
@@ -1,8 +1,5 @@
 package hep.aida.ref;
 
-import hep.aida.IHistogram1D;
-import hep.aida.IHistogram2D;
-
 /**
  * Convenient histogram utilities. 
  */
@@ -15,7 +12,7 @@
 	/** 
 	 * Returns the index of the in-range bin containing the maxBinHeight().
 	 */ 
-	public int maxBin(IHistogram1D h)
+	public int maxBin(Histogram1D h)
 	{
 		int maxBin = -1;
 		double maxValue = Double.MIN_VALUE;
@@ -31,7 +28,7 @@
 	/** 
 	 * Returns the indexX of the in-range bin containing the maxBinHeight().
 	 */ 
-	public int maxBinX(IHistogram2D h)
+	public int maxBinX(Histogram2D h)
 	{
 		double maxValue = Double.MIN_VALUE;
 		int maxBinX = -1;
@@ -51,7 +48,7 @@
 	/** 
 	 * Returns the indexY of the in-range bin containing the maxBinHeight().
 	 */ 
-	public int maxBinY(IHistogram2D h)
+	public int maxBinY(Histogram2D h)
 	{
 		double maxValue = Double.MIN_VALUE;
 		int maxBinX = -1;
@@ -71,7 +68,7 @@
 	/** 
 	 * Returns the index of the in-range bin containing the minBinHeight().
 	 */ 
-	public int minBin(IHistogram1D h)
+	public int minBin(Histogram1D h)
 	{
 		int minBin = -1;
 		double minValue = Double.MAX_VALUE;
@@ -87,7 +84,7 @@
 	/** 
 	 * Returns the indexX of the in-range bin containing the minBinHeight().
 	 */ 
-	public int minBinX(IHistogram2D h)
+	public int minBinX(Histogram2D h)
 	{
 		double minValue = Double.MAX_VALUE;
 		int minBinX = -1;
@@ -107,7 +104,7 @@
 	/** 
 	 * Returns the indexY of the in-range bin containing the minBinHeight().
 	 */ 
-	public int minBinY(IHistogram2D h)
+	public int minBinY(Histogram2D h)
 	{
 		double minValue = Double.MAX_VALUE;
 		int minBinX = -1;
--- a/src/cern/colt/matrix/doublealgo/Statistic.java
+++ b/src/cern/colt/matrix/doublealgo/Statistic.java
@@ -317,7 +317,7 @@
 @return the histogram containing the cube.
 @throws IllegalArgumentException if <tt>x.size() != y.size() || y.size() != weights.size()</tt>.
 */
-public static hep.aida.IHistogram2D cube(DoubleMatrix1D x, DoubleMatrix1D y, DoubleMatrix1D weights) {
+public static hep.aida.ref.Histogram2D cube(DoubleMatrix1D x, DoubleMatrix1D y, DoubleMatrix1D weights) {
 	if (x.size() != y.size() || y.size() != weights.size()) throw new IllegalArgumentException("vectors must have same size");
 	
 	double epsilon = 1.0E-9;
@@ -332,7 +332,7 @@
 	// since bins are right-open [from,to) we need an additional dummy bin so that the last distinct value does not fall into the overflow bin
 	if (distinct.size()>0) distinct.add(distinct.get(distinct.size()-1) + epsilon);
 	distinct.trimToSize();
-	hep.aida.IAxis xaxis = new hep.aida.ref.VariableAxis(distinct.elements());
+	hep.aida.ref.VariableAxis xaxis = new hep.aida.ref.VariableAxis(distinct.elements());
 
 	// compute distinct values of y
 	y.toArray(vals);
@@ -341,9 +341,9 @@
 	// since bins are right-open [from,to) we need an additional dummy bin so that the last distinct value does not fall into the overflow bin
 	if (distinct.size()>0) distinct.add(distinct.get(distinct.size()-1) + epsilon);
 	distinct.trimToSize();
-	hep.aida.IAxis yaxis = new hep.aida.ref.VariableAxis(distinct.elements());
+	hep.aida.ref.VariableAxis yaxis = new hep.aida.ref.VariableAxis(distinct.elements());
 
-	hep.aida.IHistogram2D histo = new hep.aida.ref.Histogram2D("Cube",xaxis,yaxis);
+	hep.aida.ref.Histogram2D histo = new hep.aida.ref.Histogram2D("Cube",xaxis,yaxis);
 	return histogram(histo,x,y,weights);
 }
 /**
@@ -356,7 +356,7 @@
 @return the histogram containing the cube.
 @throws IllegalArgumentException if <tt>x.size() != y.size() || x.size() != z.size() || x.size() != weights.size()</tt>.
 */
-public static hep.aida.IHistogram3D cube(DoubleMatrix1D x, DoubleMatrix1D y, DoubleMatrix1D z, DoubleMatrix1D weights) {
+public static hep.aida.ref.Histogram3D cube(DoubleMatrix1D x, DoubleMatrix1D y, DoubleMatrix1D z, DoubleMatrix1D weights) {
 	if (x.size() != y.size() || x.size() != z.size() || x.size() != weights.size()) throw new IllegalArgumentException("vectors must have same size");
 	
 	double epsilon = 1.0E-9;
@@ -371,7 +371,7 @@
 	// since bins are right-open [from,to) we need an additional dummy bin so that the last distinct value does not fall into the overflow bin
 	if (distinct.size()>0) distinct.add(distinct.get(distinct.size()-1) + epsilon);
 	distinct.trimToSize();
-	hep.aida.IAxis xaxis = new hep.aida.ref.VariableAxis(distinct.elements());
+	hep.aida.ref.VariableAxis xaxis = new hep.aida.ref.VariableAxis(distinct.elements());
 
 	// compute distinct values of y
 	y.toArray(vals);
@@ -380,7 +380,7 @@
 	// since bins are right-open [from,to) we need an additional dummy bin so that the last distinct value does not fall into the overflow bin
 	if (distinct.size()>0) distinct.add(distinct.get(distinct.size()-1) + epsilon);
 	distinct.trimToSize();
-	hep.aida.IAxis yaxis = new hep.aida.ref.VariableAxis(distinct.elements());
+	hep.aida.ref.VariableAxis yaxis = new hep.aida.ref.VariableAxis(distinct.elements());
 
 	// compute distinct values of z
 	z.toArray(vals);
@@ -389,9 +389,9 @@
 	// since bins are right-open [from,to) we need an additional dummy bin so that the last distinct value does not fall into the overflow bin
 	if (distinct.size()>0) distinct.add(distinct.get(distinct.size()-1) + epsilon);
 	distinct.trimToSize();
-	hep.aida.IAxis zaxis = new hep.aida.ref.VariableAxis(distinct.elements());
+	hep.aida.ref.VariableAxis zaxis = new hep.aida.ref.VariableAxis(distinct.elements());
 
-	hep.aida.IHistogram3D histo = new hep.aida.ref.Histogram3D("Cube",xaxis,yaxis,zaxis);
+	hep.aida.ref.Histogram3D histo = new hep.aida.ref.Histogram3D("Cube",xaxis,yaxis,zaxis);
 	return histogram(histo,x,y,z,weights);
 }
 /**
@@ -494,7 +494,7 @@
  * Fills all cells of the given vector into the given histogram.
  * @return <tt>histo</tt> (for convenience only).
  */
-public static hep.aida.IHistogram1D histogram(hep.aida.IHistogram1D histo, DoubleMatrix1D vector) {
+public static hep.aida.ref.Histogram1D histogram(hep.aida.ref.Histogram1D histo, DoubleMatrix1D vector) {
 	for (int i=vector.size(); --i >= 0; ) {
 		histo.fill(vector.getQuick(i));
 	}
@@ -505,7 +505,7 @@
  * @return <tt>histo</tt> (for convenience only).
  * @throws IllegalArgumentException if <tt>x.size() != y.size()</tt>.
  */
-public static hep.aida.IHistogram2D histogram(hep.aida.IHistogram2D histo, DoubleMatrix1D x, DoubleMatrix1D y) {
+public static hep.aida.ref.Histogram2D histogram(hep.aida.ref.Histogram2D histo, DoubleMatrix1D x, DoubleMatrix1D y) {
 	if (x.size() != y.size()) throw new IllegalArgumentException("vectors must have same size");
 	for (int i=x.size(); --i >= 0; ) {
 		histo.fill(x.getQuick(i), y.getQuick(i));
@@ -517,7 +517,7 @@
  * @return <tt>histo</tt> (for convenience only).
  * @throws IllegalArgumentException if <tt>x.size() != y.size() || y.size() != weights.size()</tt>.
  */
-public static hep.aida.IHistogram2D histogram(hep.aida.IHistogram2D histo, DoubleMatrix1D x, DoubleMatrix1D y, DoubleMatrix1D weights) {
+public static hep.aida.ref.Histogram2D histogram(hep.aida.ref.Histogram2D histo, DoubleMatrix1D x, DoubleMatrix1D y, DoubleMatrix1D weights) {
 	if (x.size() != y.size() || y.size() != weights.size()) throw new IllegalArgumentException("vectors must have same size");
 	for (int i=x.size(); --i >= 0; ) {
 		histo.fill(x.getQuick(i), y.getQuick(i), weights.getQuick(i));
@@ -529,7 +529,7 @@
  * @return <tt>histo</tt> (for convenience only).
  * @throws IllegalArgumentException if <tt>x.size() != y.size() || x.size() != z.size() || x.size() != weights.size()</tt>.
  */
-public static hep.aida.IHistogram3D histogram(hep.aida.IHistogram3D histo, DoubleMatrix1D x, DoubleMatrix1D y, DoubleMatrix1D z, DoubleMatrix1D weights) {
+public static hep.aida.ref.Histogram3D histogram(hep.aida.ref.Histogram3D histo, DoubleMatrix1D x, DoubleMatrix1D y, DoubleMatrix1D z, DoubleMatrix1D weights) {
 	if (x.size() != y.size() || x.size() != z.size() || x.size() != weights.size()) throw new IllegalArgumentException("vectors must have same size");
 	for (int i=x.size(); --i >= 0; ) {
 		histo.fill(x.getQuick(i), y.getQuick(i), z.getQuick(i), weights.getQuick(i));
--- a/src/hep/aida/ref/AbstractHistogram2D.java
+++ b/src/hep/aida/ref/AbstractHistogram2D.java
@@ -1,18 +1,14 @@
 package hep.aida.ref;
 
-import hep.aida.IAxis;
-import hep.aida.IHistogram;
-import hep.aida.IHistogram1D;
-import hep.aida.IHistogram2D;
 /**
 Abstract base class extracting and implementing most of the redundancy of the interface.
 
 @author Wolfgang Hoschek, Tony Johnson, and others.
 @version 1.0, 23/03/2000
 */
-abstract class AbstractHistogram2D extends Histogram implements IHistogram2D
+abstract class AbstractHistogram2D extends Histogram
 {
-	protected IAxis xAxis, yAxis;
+	protected FixedAxis xAxis, yAxis;
 	AbstractHistogram2D(String title)
 	{
 		super(title);
@@ -75,7 +71,7 @@
 	 * <b>Note 2</b>indexY1 and indexY2 may include the use of under and over flow bins
 	 * <b>Note 3</b>There is no note 3 (yet)
 	 */
-	protected abstract IHistogram1D internalSliceX(String title, int indexY1, int indexY2);
+	protected abstract Histogram1D internalSliceX(String title, int indexY1, int indexY2);
 	/**
 	 * The precise meaning of the arguments to the public slice
 	 * methods is somewhat ambiguous, so we define this internal
@@ -86,7 +82,7 @@
 	 * <b>Note 2</b>indexX1 and indexX2 may include the use of under and over flow bins
 	 * <b>Note 3</b>There is no note 3 (yet)
 	 */
-	protected abstract IHistogram1D internalSliceY(String title, int indexX1, int indexX2);
+	protected abstract Histogram1D internalSliceY(String title, int indexX1, int indexX2);
 /**
 	 * Package private method to map from the external representation of bin
 	 * number to our internal representation of bin number
@@ -96,8 +92,8 @@
 		int bins = xAxis.bins() + 2;
 		if (index >= bins) throw new IllegalArgumentException("bin="+index);
 		if (index >= 0) return index+1;
-		if (index == IHistogram.UNDERFLOW) return 0;
-		if (index == IHistogram.OVERFLOW) return bins-1;
+		if (index == Histogram.UNDERFLOW) return 0;
+		if (index == Histogram.OVERFLOW) return bins-1;
 		throw new IllegalArgumentException("bin="+index);
 	}
 /**
@@ -109,8 +105,8 @@
 		int bins = yAxis.bins() + 2;
 		if (index >= bins) throw new IllegalArgumentException("bin="+index);
 		if (index >= 0) return index+1;
-		if (index == IHistogram.UNDERFLOW) return 0;
-		if (index == IHistogram.OVERFLOW) return bins-1;
+		if (index == Histogram.UNDERFLOW) return 0;
+		if (index == Histogram.OVERFLOW) return bins-1;
 		throw new IllegalArgumentException("bin="+index);
 	}
 	public int[] minMaxBins()
@@ -139,26 +135,26 @@
 		int[] result = {minBinX,minBinY, maxBinX,maxBinY};
 		return result;
 	}
-	public IHistogram1D projectionX()
+	public Histogram1D projectionX()
 	{
 		String newTitle = title() + " (projectionX)";
 		//return internalSliceX(newTitle,yAxis.under,yAxis.over);
-		return internalSliceX(newTitle,mapY(IHistogram.UNDERFLOW),mapY(IHistogram.OVERFLOW));
+		return internalSliceX(newTitle,mapY(Histogram.UNDERFLOW),mapY(Histogram.OVERFLOW));
 	}
-	public IHistogram1D projectionY()
+	public Histogram1D projectionY()
 	{
 		String newTitle = title() + " (projectionY)";
 		//return internalSliceY(newTitle,xAxis.under,xAxis.over);
-		return internalSliceY(newTitle,mapX(IHistogram.UNDERFLOW),mapX(IHistogram.OVERFLOW));
+		return internalSliceY(newTitle,mapX(Histogram.UNDERFLOW),mapX(Histogram.OVERFLOW));
 	}
-	public IHistogram1D sliceX(int indexY)
+	public Histogram1D sliceX(int indexY)
 	{
 		//int start = yAxis.map(indexY);
 		int start = mapY(indexY);
 		String newTitle = title() + " (sliceX ["+indexY+"])";
 		return internalSliceX(newTitle,start,start);
 	}
-	public IHistogram1D sliceX(int indexY1, int indexY2)
+	public Histogram1D sliceX(int indexY1, int indexY2)
 	{
 		//int start = yAxis.map(indexY1);
 		//int stop = yAxis.map(indexY2);
@@ -167,14 +163,14 @@
 		String newTitle = title() + " (sliceX ["+indexY1+":"+indexY2+"])";
 		return internalSliceX(newTitle,start,stop);
 	}
-	public IHistogram1D sliceY(int indexX)
+	public Histogram1D sliceY(int indexX)
 	{
 		//int start = xAxis.map(indexX);
 		int start = mapX(indexX);
 		String newTitle = title() + " (sliceY ["+indexX+"])";
 		return internalSliceY(newTitle,start,start);
 	}
-	public IHistogram1D sliceY(int indexX1, int indexX2)
+	public Histogram1D sliceY(int indexX1, int indexX2)
 	{
 		//int start = xAxis.map(indexX1);
 		//int stop = xAxis.map(indexX2);
@@ -207,12 +203,15 @@
 	{
 		return sumAllBinHeights() - sumBinHeights();
 	}
-	public IAxis xAxis()
+	public FixedAxis xAxis()
 	{
 		return xAxis;
 	}
-	public IAxis yAxis()
+	public FixedAxis yAxis()
 	{
 		return yAxis;
 	}
+	public abstract double binHeight(int indexX, int indexY);
+	public abstract int binEntries(int indexX, int indexY);
+	public abstract void fill(double x, double y, double weight);
 }
--- a/src/hep/aida/ref/Histogram.java
+++ b/src/hep/aida/ref/Histogram.java
@@ -6,7 +6,7 @@
 @author Wolfgang Hoschek, Tony Johnson, and others.
 @version 1.0, 23/03/2000
 */
-abstract class Histogram implements hep.aida.IHistogram
+abstract class Histogram
 {
 	private String title;
 	Histogram(String title)
@@ -17,4 +17,13 @@
 	{
 		return title;
 	}
+
+	/**
+	 * Constant specifying the overflow bin (can be passed to any method expecting a bin number).
+	 */
+	public final static int OVERFLOW = -1;
+	/**
+	 * Constant specifying the underflow bin (can be passed to any method expecting a bin number).
+	 */
+	public final static int UNDERFLOW = -2;
 }
--- a/src/hep/aida/ref/Histogram2D.java
+++ b/src/hep/aida/ref/Histogram2D.java
@@ -1,8 +1,5 @@
 package hep.aida.ref;
 
-import hep.aida.IAxis;
-import hep.aida.IHistogram1D;
-import hep.aida.IHistogram2D;
 /**
 A reference implementation of hep.aida.IHistogram2D.
 The goal is to provide a clear implementation rather than the most efficient implementation.
@@ -11,7 +8,7 @@
 @author Wolfgang Hoschek, Tony Johnson, and others.
 @version 1.0, 23/03/2000
 */
-public class Histogram2D extends AbstractHistogram2D implements IHistogram2D
+public class Histogram2D extends AbstractHistogram2D
 {
 	private double[][] heights;
 	private double[][] errors;
@@ -58,7 +55,7 @@
 	 * @param xAxis The x-axis description to be used for binning.
 	 * @param yAxis The y-axis description to be used for binning.
 	 */
-	public Histogram2D(String title, IAxis xAxis, IAxis yAxis)
+	public Histogram2D(String title, FixedAxis xAxis, FixedAxis yAxis)
 	{
 		super(title);
 		this.xAxis = xAxis;
@@ -138,7 +135,7 @@
 	 * <b>Note 2</b>indexY1 and indexY2 may include the use of under and over flow bins
 	 * <b>Note 3</b>There is no note 3 (yet)
 	 */
-	protected IHistogram1D internalSliceX(String title, int indexY1, int indexY2)
+	protected Histogram1D internalSliceX(String title, int indexY1, int indexY2)
 	{
 		// Attention: our internal definition of bins has been choosen
 		// so that this works properly even if the indeces passed in include
@@ -174,7 +171,7 @@
 	 * <b>Note 2</b>indexX1 and indexX2 may include the use of under and over flow bins
 	 * <b>Note 3</b>There is no note 3 (yet)
 	 */
-	protected IHistogram1D internalSliceY(String title, int indexX1, int indexX2)
+	protected Histogram1D internalSliceY(String title, int indexX1, int indexX2)
 	{
 		// Attention: our internal definition of bins has been choosen
 		// so that this works properly even if the indeces passed in include
--- a/src/hep/aida/ref/AbstractHistogram3D.java
+++ b/src/hep/aida/ref/AbstractHistogram3D.java
@@ -1,18 +1,14 @@
 package hep.aida.ref;
 
-import hep.aida.IAxis;
-import hep.aida.IHistogram;
-import hep.aida.IHistogram2D;
-import hep.aida.IHistogram3D;
 /**
 Abstract base class extracting and implementing most of the redundancy of the interface.
 
 @author Wolfgang Hoschek, Tony Johnson, and others.
 @version 1.0, 23/03/2000
 */
-abstract class AbstractHistogram3D extends Histogram implements IHistogram3D
+abstract class AbstractHistogram3D extends Histogram
 {
-	protected IAxis xAxis, yAxis, zAxis;
+	protected FixedAxis xAxis, yAxis, zAxis;
 	AbstractHistogram3D(String title)
 	{
 		super(title);
@@ -61,7 +57,7 @@
 	 * <b>Note 2</b>indexX1 and indexX2 may include the use of under and over flow bins
 	 * <b>Note 3</b>There is no note 3 (yet)
 	 */
-	protected abstract IHistogram2D internalSliceXY(String title, int indexZ1, int indexZ2);
+	protected abstract Histogram2D internalSliceXY(String title, int indexZ1, int indexZ2);
 	/**
 	 * The precise meaning of the arguments to the public slice
 	 * methods is somewhat ambiguous, so we define this internal
@@ -72,7 +68,7 @@
 	 * <b>Note 2</b>indexY1 and indexY2 may include the use of under and over flow bins
 	 * <b>Note 3</b>There is no note 3 (yet)
 	 */
-	protected abstract IHistogram2D internalSliceXZ(String title, int indexY1, int indexY2);
+	protected abstract Histogram2D internalSliceXZ(String title, int indexY1, int indexY2);
 	/**
 	 * The precise meaning of the arguments to the public slice
 	 * methods is somewhat ambiguous, so we define this internal
@@ -83,7 +79,7 @@
 	 * <b>Note 2</b>indexX1 and indexX2 may include the use of under and over flow bins
 	 * <b>Note 3</b>There is no note 3 (yet)
 	 */
-	protected abstract IHistogram2D internalSliceYZ(String title, int indexX1, int indexX2);
+	protected abstract Histogram2D internalSliceYZ(String title, int indexX1, int indexX2);
 	/**
 	 * Package private method to map from the external representation of bin
 	 * number to our internal representation of bin number
@@ -93,8 +89,8 @@
 		int bins = xAxis.bins() + 2;
 		if (index >= bins) throw new IllegalArgumentException("bin="+index);
 		if (index >= 0) return index+1;
-		if (index == IHistogram.UNDERFLOW) return 0;
-		if (index == IHistogram.OVERFLOW) return bins-1;
+		if (index == Histogram.UNDERFLOW) return 0;
+		if (index == Histogram.OVERFLOW) return bins-1;
 		throw new IllegalArgumentException("bin="+index);
 	}
 	/**
@@ -106,8 +102,8 @@
 		int bins = yAxis.bins() + 2;
 		if (index >= bins) throw new IllegalArgumentException("bin="+index);
 		if (index >= 0) return index+1;
-		if (index == IHistogram.UNDERFLOW) return 0;
-		if (index == IHistogram.OVERFLOW) return bins-1;
+		if (index == Histogram.UNDERFLOW) return 0;
+		if (index == Histogram.OVERFLOW) return bins-1;
 		throw new IllegalArgumentException("bin="+index);
 	}
 	/**
@@ -119,8 +115,8 @@
 		int bins = zAxis.bins() + 2;
 		if (index >= bins) throw new IllegalArgumentException("bin="+index);
 		if (index >= 0) return index+1;
-		if (index == IHistogram.UNDERFLOW) return 0;
-		if (index == IHistogram.OVERFLOW) return bins-1;
+		if (index == Histogram.UNDERFLOW) return 0;
+		if (index == Histogram.OVERFLOW) return bins-1;
 		throw new IllegalArgumentException("bin="+index);
 	}
 	public int[] minMaxBins()
@@ -155,48 +151,48 @@
 		int[] result = {minBinX,minBinY,minBinZ, maxBinX,maxBinY,maxBinZ};
 		return result;
 	}
-	public IHistogram2D projectionXY()
+	public Histogram2D projectionXY()
 	{
 		String newTitle = title() + " (projectionXY)";
-		return internalSliceXY(newTitle,mapZ(IHistogram.UNDERFLOW),mapZ(IHistogram.OVERFLOW));
+		return internalSliceXY(newTitle,mapZ(Histogram.UNDERFLOW),mapZ(Histogram.OVERFLOW));
 	}
-	public IHistogram2D projectionXZ()
+	public Histogram2D projectionXZ()
 	{
 		String newTitle = title() + " (projectionXZ)";
-		return internalSliceXZ(newTitle,mapY(IHistogram.UNDERFLOW),mapY(IHistogram.OVERFLOW));
+		return internalSliceXZ(newTitle,mapY(Histogram.UNDERFLOW),mapY(Histogram.OVERFLOW));
 	}
-	public IHistogram2D projectionYZ()
+	public Histogram2D projectionYZ()
 	{
 		String newTitle = title() + " (projectionYZ)";
-		return internalSliceYZ(newTitle,mapX(IHistogram.UNDERFLOW),mapX(IHistogram.OVERFLOW));
+		return internalSliceYZ(newTitle,mapX(Histogram.UNDERFLOW),mapX(Histogram.OVERFLOW));
 	}
-	public IHistogram2D sliceXY(int indexZ )
+	public Histogram2D sliceXY(int indexZ )
 	{
 		return sliceXY(indexZ,indexZ);
 	}
-	public IHistogram2D sliceXY(int indexZ1, int indexZ2)
+	public Histogram2D sliceXY(int indexZ1, int indexZ2)
 	{
 		int start = mapZ(indexZ1);
 		int stop = mapZ(indexZ2);
 		String newTitle = title() + " (sliceXY ["+indexZ1+":"+indexZ2+"])";
 		return internalSliceXY(newTitle,start,stop);
 	}
-	public IHistogram2D sliceXZ(int indexY )
+	public Histogram2D sliceXZ(int indexY )
 	{
 		return sliceXZ(indexY,indexY);
 	}
-	public IHistogram2D sliceXZ(int indexY1, int indexY2)
+	public Histogram2D sliceXZ(int indexY1, int indexY2)
 	{
 		int start = mapY(indexY1);
 		int stop = mapY(indexY2);
 		String newTitle = title() + " (sliceXZ ["+indexY1+":"+indexY2+"])";
 		return internalSliceXY(newTitle,start,stop);
 	}
-	public IHistogram2D sliceYZ(int indexX )
+	public Histogram2D sliceYZ(int indexX )
 	{
 		return sliceYZ(indexX,indexX);
 	}
-	public IHistogram2D sliceYZ(int indexX1, int indexX2)
+	public Histogram2D sliceYZ(int indexX1, int indexX2)
 	{
 		int start = mapX(indexX1);
 		int stop = mapX(indexX2);
@@ -229,16 +225,19 @@
 	{
 		return sumAllBinHeights() - sumBinHeights();
 	}
-	public IAxis xAxis()
+	public FixedAxis xAxis()
 	{
 		return xAxis;
 	}
-	public IAxis yAxis()
+	public FixedAxis yAxis()
 	{
 		return yAxis;
 	}
-	public IAxis zAxis()
+	public FixedAxis zAxis()
 	{
 		return zAxis;
 	}
+	public abstract double binHeight(int indexX, int indexY, int indexZ);
+	public abstract int binEntries(int indexX, int indexY, int indexZ);
+	public abstract void fill(double x, double y, double z, double weight);
 }
--- a/src/hep/aida/ref/Histogram3D.java
+++ b/src/hep/aida/ref/Histogram3D.java
@@ -1,8 +1,5 @@
 package hep.aida.ref;
 
-import hep.aida.IAxis;
-import hep.aida.IHistogram2D;
-import hep.aida.IHistogram3D;
 /**
 A reference implementation of hep.aida.IHistogram3D.
 The goal is to provide a clear implementation rather than the most efficient implementation.
@@ -11,7 +8,7 @@
 @author Wolfgang Hoschek, Tony Johnson, and others.
 @version 1.0, 23/03/2000
 */
-public class Histogram3D extends AbstractHistogram3D implements IHistogram3D
+public class Histogram3D extends AbstractHistogram3D
 {
 	private double[][][] heights;
 	private double[][][] errors;
@@ -66,7 +63,7 @@
 	 * @param yAxis The y-axis description to be used for binning.
 	 * @param zAxis The z-axis description to be used for binning.
 	 */
-	public Histogram3D(String title, IAxis xAxis, IAxis yAxis, IAxis zAxis)
+	public Histogram3D(String title, FixedAxis xAxis, FixedAxis yAxis, FixedAxis zAxis)
 	{
 		super(title);
 		this.xAxis = xAxis;
@@ -147,7 +144,7 @@
 	 * <b>Note 2</b>indexX1 and indexX2 may include the use of under and over flow bins
 	 * <b>Note 3</b>There is no note 3 (yet)
 	 */
-	protected IHistogram2D internalSliceXY(String title, int indexZ1, int indexZ2)
+	protected Histogram2D internalSliceXY(String title, int indexZ1, int indexZ2)
 	{
 		// Attention: our internal definition of bins has been choosen
 		// so that this works properly even if the indeces passed in include
@@ -186,7 +183,7 @@
 	 * <b>Note 2</b>indexY1 and indexY2 may include the use of under and over flow bins
 	 * <b>Note 3</b>There is no note 3 (yet)
 	 */
-	protected IHistogram2D internalSliceXZ(String title, int indexY1, int indexY2)
+	protected Histogram2D internalSliceXZ(String title, int indexY1, int indexY2)
 	{
 		// Attention: our internal definition of bins has been choosen
 		// so that this works properly even if the indeces passed in include
@@ -225,7 +222,7 @@
 	 * <b>Note 2</b>indexX1 and indexX2 may include the use of under and over flow bins
 	 * <b>Note 3</b>There is no note 3 (yet)
 	 */
-	protected IHistogram2D internalSliceYZ(String title, int indexX1, int indexX2)
+	protected Histogram2D internalSliceYZ(String title, int indexX1, int indexX2)
 	{
 		// Attention: our internal definition of bins has been choosen
 		// so that this works properly even if the indeces passed in include
--- a/src/hep/aida/ref/FixedAxis.java
+++ b/src/hep/aida/ref/FixedAxis.java
@@ -1,14 +1,12 @@
 package hep.aida.ref;
 
-import hep.aida.IAxis;
-import hep.aida.IHistogram;
 /**
 Fixed-width axis; A reference implementation of hep.aida.IAxis.
 
 @author Wolfgang Hoschek, Tony Johnson, and others.
 @version 1.0, 23/03/2000
 */
-public class FixedAxis implements IAxis
+public class FixedAxis implements java.io.Serializable
 {
 	private int bins;
 	private double min;
@@ -41,14 +39,15 @@
 		//this.over = bins+1;
 
 	}
+	FixedAxis() {  } //Null constructor for VariableAxis
 	public double binCentre(int index)
 	{
 		return min + binWidth*index + binWidth/2;
 	}
 	public double binLowerEdge(int index)
 	{
-		if (index == IHistogram.UNDERFLOW) return Double.NEGATIVE_INFINITY;
-		if (index == IHistogram.OVERFLOW) return upperEdge();
+		if (index == Histogram.UNDERFLOW) return Double.NEGATIVE_INFINITY;
+		if (index == Histogram.OVERFLOW) return upperEdge();
 		return min + binWidth*index;
 	}
 	public int bins()
@@ -57,8 +56,8 @@
 	}
 	public double binUpperEdge(int index)
 	{
-		if (index == IHistogram.UNDERFLOW) return min;
-		if (index == IHistogram.OVERFLOW) return Double.POSITIVE_INFINITY;
+		if (index == Histogram.UNDERFLOW) return min;
+		if (index == Histogram.OVERFLOW) return Double.POSITIVE_INFINITY;
 		return min + binWidth*(index+1);
 	}
 	public double binWidth(int index)
@@ -67,9 +66,9 @@
 	}
 	public int coordToIndex(double coord)
 	{
-		if (coord < min) return IHistogram.UNDERFLOW;
+		if (coord < min) return Histogram.UNDERFLOW;
 		int index = (int) Math.floor((coord - min)/binWidth);
-		if (index >= bins) return IHistogram.OVERFLOW;
+		if (index >= bins) return Histogram.OVERFLOW;
 		
 		return index;
 	}
@@ -100,8 +99,8 @@
 	{
 		if (index >= bins) throw new IllegalArgumentException("bin="+index);
 		if (index >= 0) return index+1;
-		if (index == IHistogram.UNDERFLOW) return xunder;
-		if (index == IHistogram.OVERFLOW) return xover;
+		if (index == Histogram.UNDERFLOW) return xunder;
+		if (index == Histogram.OVERFLOW) return xover;
 		throw new IllegalArgumentException("bin="+index);
 	}
 }
--- a/src/hep/aida/ref/VariableAxis.java
+++ b/src/hep/aida/ref/VariableAxis.java
@@ -1,14 +1,12 @@
 package hep.aida.ref;
 
-import hep.aida.IAxis;
-import hep.aida.IHistogram;
 /**
 Variable-width axis; A reference implementation of hep.aida.IAxis.
 
 @author Wolfgang Hoschek, Tony Johnson, and others.
 @version 1.0, 23/03/2000
 */
-public class VariableAxis implements IAxis 
+public class VariableAxis extends FixedAxis
 {
 	protected double min; 
 	protected int bins;
@@ -41,8 +39,8 @@
 	}
 	public double binLowerEdge(int index)
 	{
-		if (index == IHistogram.UNDERFLOW) return Double.NEGATIVE_INFINITY;
-		if (index == IHistogram.OVERFLOW) return upperEdge();
+		if (index == Histogram.UNDERFLOW) return Double.NEGATIVE_INFINITY;
+		if (index == Histogram.OVERFLOW) return upperEdge();
 		return edges[index];
 	}
 	public int bins()
@@ -51,8 +49,8 @@
 	}
 	public double binUpperEdge(int index)
 	{
-		if (index == IHistogram.UNDERFLOW) return lowerEdge();
-		if (index == IHistogram.OVERFLOW) return Double.POSITIVE_INFINITY;
+		if (index == Histogram.UNDERFLOW) return lowerEdge();
+		if (index == Histogram.OVERFLOW) return Double.POSITIVE_INFINITY;
 		return edges[index+1];
 	}
 	public double binWidth(int index)
@@ -61,14 +59,14 @@
 	}
 	public int coordToIndex(double coord)
 	{
-		if (coord < min) return IHistogram.UNDERFLOW;
+		if (coord < min) return Histogram.UNDERFLOW;
 		
 		int	index = java.util.Arrays.binarySearch(this.edges,coord);
 		//int index = new DoubleArrayList(this.edges).binarySearch(coord); // just for debugging
 		if (index < 0) index = -index -1-1; // not found
 		//else index++; // found
 
-		if (index >= bins) return IHistogram.OVERFLOW;
+		if (index >= bins) return Histogram.OVERFLOW;
 		
 		return index;
 	}
--- a/src/hep/aida/ref/AbstractHistogram1D.java
+++ b/src/hep/aida/ref/AbstractHistogram1D.java
@@ -1,17 +1,14 @@
 package hep.aida.ref;
 
-import hep.aida.IAxis;
-import hep.aida.IHistogram;
-import hep.aida.IHistogram1D;
 /**
 Abstract base class extracting and implementing most of the redundancy of the interface.
 
 @author Wolfgang Hoschek, Tony Johnson, and others.
 @version 1.0, 23/03/2000
 */
-abstract class AbstractHistogram1D extends Histogram implements IHistogram1D
+abstract class AbstractHistogram1D extends Histogram
 {
-	protected IAxis xAxis;
+	protected FixedAxis xAxis;
 	AbstractHistogram1D(String title)
 	{
 		super(title);
@@ -44,8 +41,8 @@
 		int bins = xAxis.bins()+2;
 		if (index >= bins) throw new IllegalArgumentException("bin="+index);
 		if (index >= 0) return index+1;
-		if (index == IHistogram.UNDERFLOW) return 0;
-		if (index == IHistogram.OVERFLOW) return bins-1;
+		if (index == Histogram.UNDERFLOW) return 0;
+		if (index == Histogram.OVERFLOW) return bins-1;
 		throw new IllegalArgumentException("bin="+index);
 	}
 	public int[] minMaxBins()
@@ -83,8 +80,10 @@
 		return binHeight(UNDERFLOW) + binHeight(OVERFLOW);
 		//return heights[xAxis.under] + heights[xAxis.over];
 	}
-	public IAxis xAxis()
+	public FixedAxis xAxis()
 	{
 		return xAxis;
 	}
+	public abstract double binHeight(int index);
+	public abstract int binEntries(int index);
 }
--- a/src/hep/aida/ref/Converter.java
+++ b/src/hep/aida/ref/Converter.java
@@ -1,10 +1,5 @@
 package hep.aida.ref;
 
-import hep.aida.IAxis;
-import hep.aida.IHistogram1D;
-import hep.aida.IHistogram2D;
-import hep.aida.IHistogram3D;
-
 /**
  * Histogram conversions, for example to String and XML format; 
  * This class requires the Colt distribution, whereas the rest of the package is entirelly stand-alone.
@@ -18,7 +13,7 @@
 	/** 
 	 * Returns all edges of the given axis.
 	 */ 
-	public double[] edges(IAxis axis)
+	public double[] edges(FixedAxis axis)
 	{
 		int b = axis.bins();
 		double[] bounds = new double[b+1];
@@ -33,7 +28,7 @@
 	/** 
 	 * Returns an array[h.xAxis().bins()]; ignoring extra bins.
 	 */ 
-	protected double[] toArrayErrors(IHistogram1D h)
+	protected double[] toArrayErrors(Histogram1D h)
 	{
 		int xBins = h.xAxis().bins();
 		double[] array = new double[xBins];
@@ -45,7 +40,7 @@
 	/** 
 	 * Returns an array[h.xAxis().bins()][h.yAxis().bins()]; ignoring extra bins.
 	 */ 
-	protected double[][] toArrayErrors(IHistogram2D h)
+	protected double[][] toArrayErrors(Histogram2D h)
 	{
 		int xBins = h.xAxis().bins();
 		int yBins = h.yAxis().bins();
@@ -60,7 +55,7 @@
 	/** 
 	 * Returns an array[h.xAxis().bins()]; ignoring extra bins.
 	 */ 
-	protected double[] toArrayHeights(IHistogram1D h)
+	protected double[] toArrayHeights(Histogram1D h)
 	{
 		int xBins = h.xAxis().bins();
 		double[] array = new double[xBins];
@@ -72,7 +67,7 @@
 	/** 
 	 * Returns an array[h.xAxis().bins()][h.yAxis().bins()]; ignoring extra bins.
 	 */ 
-	protected double[][] toArrayHeights(IHistogram2D h)
+	protected double[][] toArrayHeights(Histogram2D h)
 	{
 		int xBins = h.xAxis().bins();
 		int yBins = h.yAxis().bins();
@@ -87,7 +82,7 @@
 	/** 
 	 * Returns an array[h.xAxis().bins()][h.yAxis().bins()][h.zAxis().bins()]; ignoring extra bins.
 	 */ 
-	protected double[][][] toArrayHeights(IHistogram3D h)
+	protected double[][][] toArrayHeights(Histogram3D h)
 	{
 		int xBins = h.xAxis().bins();
 		int yBins = h.yAxis().bins();
@@ -124,7 +119,7 @@
 	/** 
 	 * Returns a string representation of the given argument.
 	 */ 
-	public String toString(IAxis axis) 
+	public String toString(FixedAxis axis) 
 	{
 		StringBuffer buf = new StringBuffer();
 		buf.append("Range: ["+axis.lowerEdge()+","+axis.upperEdge()+")");
@@ -135,7 +130,7 @@
 	/** 
 	 * Returns a string representation of the given argument.
 	 */ 
-	public String toString(IHistogram1D h)
+	public String toString(Histogram1D h)
 	{		
 		String columnAxisName = null; //"X";
 		String rowAxisName = null;
@@ -179,7 +174,7 @@
 	/** 
 	 * Returns a string representation of the given argument.
 	 */ 
-	public String toString(IHistogram2D h)
+	public String toString(Histogram2D h)
 	{		
 		String columnAxisName = "X";
 		String rowAxisName = "Y";
@@ -235,7 +230,7 @@
 	/** 
 	 * Returns a string representation of the given argument.
 	 */ 
-	public String toString(IHistogram3D h)
+	public String toString(Histogram3D h)
 	{		
 		String columnAxisName = "X";
 		String rowAxisName = "Y";
@@ -302,7 +297,7 @@
 	/** 
 	 * Returns a XML representation of the given argument.
 	 */ 
-	public String toXML(IHistogram1D h)
+	public String toXML(Histogram1D h)
 	{
 			StringBuffer buf = new StringBuffer();
 			String sep = System.getProperty("line.separator");
@@ -343,7 +338,7 @@
 	/** 
 	 * Returns a XML representation of the given argument.
 	 */ 
-	public String toXML(IHistogram2D h)
+	public String toXML(Histogram2D h)
 	{
 			StringBuffer out = new StringBuffer();
 			String sep = System.getProperty("line.separator");
--- a/src/hep/aida/ref/Histogram1D.java
+++ b/src/hep/aida/ref/Histogram1D.java
@@ -1,7 +1,5 @@
 package hep.aida.ref;
 
-import hep.aida.IAxis;
-import hep.aida.IHistogram1D;
 /**
 A reference implementation of hep.aida.IHistogram1D.
 The goal is to provide a clear implementation rather than the most efficient implementation.
@@ -10,7 +8,7 @@
 @author Wolfgang Hoschek, Tony Johnson, and others.
 @version 1.0, 23/03/2000
 */
-public class Histogram1D extends AbstractHistogram1D implements IHistogram1D
+public class Histogram1D extends AbstractHistogram1D
 {
 	private double[] errors;
 	private double[] heights;
@@ -49,7 +47,7 @@
 	 * @param title The histogram title.
 	 * @param axis The axis description to be used for binning.
 	 */
-	public Histogram1D(String title, IAxis axis)
+	public Histogram1D(String title, FixedAxis axis)
 	{
 		super(title);
 		xAxis = axis;
--- a/src/hep/aida/bin/QuantileBin1D.java
+++ b/src/hep/aida/bin/QuantileBin1D.java
@@ -936,7 +936,7 @@
 @param axis an axis defining interval boundaries.
 @param resolution a measure of accuracy; the desired number of subintervals per interval. 
 */
-public synchronized MightyStaticBin1D[] splitApproximately(hep.aida.IAxis axis, int k) {
+public synchronized MightyStaticBin1D[] splitApproximately(hep.aida.ref.FixedAxis axis, int k) {
 	DoubleArrayList percentages = new DoubleArrayList(new hep.aida.ref.Converter().edges(axis));
 	percentages.beforeInsert(0,Double.NEGATIVE_INFINITY);
 	percentages.add(Double.POSITIVE_INFINITY);
